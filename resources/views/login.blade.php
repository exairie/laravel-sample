<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <link rel="stylesheet" href="{{ URL::asset('css/login.css') }}">
    <title>Document</title>
</head>

<body>
    <div class="loginwindow">
        <div class="loginheader">Login</div>
        <div class="logincontent">
            @if(\Session::get('error'))
            <div class="error">{{ \Session::get('error') }}</div>
            @endif
            <form method="POST" action="{{ URL::action('LoginController@login') }}">
                @csrf
                <table>
                    <tbody>
                        <tr>
                            <td><label for="">Username</label></td>
                            <td><input type="text" name="username"></td>
                        </tr>
                        <tr>
                            <td><label for="">Password</label></td>
                            <td><input type="text" name="password"></td>
                        </tr>
                    </tbody>
                </table>
                <button>Login</button>
            </form>
        </div>
    </div>
</body>

</html>
