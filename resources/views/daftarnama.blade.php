@extends('masterview')
@section('halamanasli')
<link rel="stylesheet" href="{{ URL::asset('css/app.css') }}">
<table border=1 style="width:100%">
    <thead>
        <th>Nama</th>
        <th>Kelas</th>
        <th>Alamat</th>
        <th>Hapus</th>
    </thead>
    <tbody>
        @foreach($data as $d)
        <tr>
            <td>{{ $d->nama }}</td>
            <td>{{ $d->kelas }}</td>
            <td>{{ $d->alamat }}</td>
            <td>
                <a href="{{ URL::action('ControllerSaya@editData',[$d->id]) }}">Edit</a>
                <a href="{{
                URL::action
                ('ControllerSaya@cobaDelete',[$d->id])
                }}">Hapus</a></td>
        </tr>
        @endforeach
    </tbody>
</table>
@endsection

@section('form')
<form action="{{
    isset($edit)?
    URL::action('ControllerSaya@saveEdit',[$edit->id])
    :
    URL::action('ControllerSaya@saveData')
    }}" method="POST">
    @csrf
    <label for="">Nama</label>
    <input value="{{ isset($edit)?$edit->nama:'' }}" type="text" name="nama"><br>
    <label for="">Kelas</label>
    <input value="{{ isset($edit)?$edit->kelas:'' }}" type="text" name="kelas"><br>
    <label for="">Alamat</label>
    <input value="{{ isset($edit)?$edit->alamat:'' }}" type="text" name="alamat">

    <input type="submit" name="submit">
</form>
@endsection
