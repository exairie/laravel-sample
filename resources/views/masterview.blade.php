<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <style>
        * {
            padding: 0;
            margin: 0;
            clear: both;
        }

        .navbar {
            background: blue;
            min-height: 35px;
            padding: 8px;
        }

        .navbar .nav {
            list-style: none;
            padding: 0;
        }

        .navbar .nav li {
            display: inline-block;
        }

        .navbar .nav li a {
            display: block;
            padding: 8px;
            text-decoration: none;
            color: white;
            text-transform: uppercase;
        }
    </style>

</head>

<body>
    <h1>GAJAH SEGEDE HEADER</h1>
    @yield('halamanasli')
    <hr>
    @yield('form')
</body>

</html>
