<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/panggiljoni', function () {
    echo "Woy jon, dipanggil!"; // <-- Anonymous function
});

Route::get(
    '/panggiljonilagi',
    'ControllerSaya@panggilJoni'
);
Route::get(
    '/nama',
    'ControllerSaya@ambilNama'
);

Route::get('/test', function () {
    echo "TEST";
});
/** Baru looh */
Route::get('/bahri', 'ControllerSaya@getData');
Route::post('/tambah', 'ControllerSaya@saveData');
Route::get('/insert', 'ControllerSaya@cobaInsert');
Route::get(
    '/delete/{id}',
    'ControllerSaya@cobaDelete'
);

Route::get(
    '/nama/{id}/edit',
    'ControllerSaya@editData'
);
Route::post(
    '/nama/{id}/edit/save',
    'ControllerSaya@saveEdit'
);

Route::get('/login', 'LoginController@loginpage');
Route::post('/login', 'LoginController@login');
Route::get('/logout', 'LoginController@logout');

Route::group(['middleware' => ['login']], function () {
    Route::get('/home', 'HomeController@index');
    Route::get('/barang', 'HomeController@index');
    Route::get('/kategori', 'HomeController@index');
    Route::get('/transaksi', 'HomeController@index');
});
