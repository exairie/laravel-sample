<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ModelNama extends Model
{
    protected $table = "nama";
    protected $fillable = [
        'nama', 'kelas', 'alamat'
    ];
}
