<?php

namespace App\Http\Middleware;

use Closure;
use App\Models\Login;

class LoginMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public static $loggedUser = null;
    public function handle($request, Closure $next)
    {
        $login_id = $request->session()->get('login_id');
        if ($login_id == null) {
            return redirect()->action('LoginController@loginpage');
        }

        $logindata = Login::find($login_id);
        if ($login_id == null) {
            return redirect()->action('LoginController@loginpage');
        }



        self::$loggedUser = $logindata;

        return $next($request);
    }
}
