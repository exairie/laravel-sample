<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Login;

class LoginController extends Controller
{
    public function login(Request $r)
    {
        // Test Tambah Comment
        $check = Login::where('username', $r->input('username'))
            ->where('password', $r->input('password'))
            ->first();
        // $check = mysql_query(
        //     "SELECT * FROM logins
        //     where username = '$username' and
        //     password = '$password' ")

        if ($check != null) {
            // Login berhasil
            $r->session()->put('login_id', $check->id);

            return redirect()->action('HomeController@index');
        } else {
            $r->session()->put('login_id', null);
            $r->session()->flash('error', 'Username and/or Password Incorrect!');
            return redirect()->action('LoginController@loginpage');
        }
    }
    public function loginpage()
    {
        return view('login');
    }
    public function logout(Request $r)
    {
        $r->session()->forget('login_id');
        return redirect()->action('LoginController@loginpage');
    }
}
