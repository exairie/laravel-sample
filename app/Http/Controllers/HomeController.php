<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Login;

class HomeController extends Controller
{
    public function index(Request $r)
    {
        return view('halaman_awal');
    }
}
