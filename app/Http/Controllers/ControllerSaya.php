<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use stdClass;
use App\Models\ModelNama;

class ControllerSaya extends Controller
{
    public function panggilJoni()
    {
        echo "Woi jon, budeg lu ya?";
    }

    public function ambilNama()
    {
        $data = \DB::select("SELECT * FROM nama");
        return view('daftarnama', ['data' => $data]);
    }
    public function getData()
    {
        $data = ModelNama::all();
        return view('daftarnama', [
            'data' => $data
        ]);
    }
    public function cobaInsert()
    {
        $insert = new ModelNama();
        $insert->nama = "Bambang";
        $insert->kelas = "RPL";
        $insert->alamat = "Cibinong";
        $insert->save();

        var_dump($insert);
    }
    public function saveData(Request $r)
    {
        $nama = $r->input('nama');
        $kelas = $r->input('kelas');
        $alamat = $r->input('alamat');

        $insert = new ModelNama();
        $insert->nama = $nama;
        $insert->kelas = $kelas;
        $insert->alamat = $alamat;
        $insert->save();

        return redirect()->action(
            'ControllerSaya@ambilNama'
        );
    }
    function cobaDelete(ModelNama $id)
    {
        // mysql_query("DELETE FROM nama WHERE id = $id");
        // header('location: tampil.php')
        $id->delete();
        return redirect()->action(
            'ControllerSaya@ambilNama'
        );
    }
    function editData(ModelNama $id)
    {
        $data = ModelNama::all();
        return view('daftarnama', [
            'data' => $data,
            'edit' => $id
        ]);
    }
    function saveEdit(Request $r, ModelNama $id)
    {
        $id->nama = $r->input('nama');
        $id->kelas = $r->input('kelas');
        $id->alamat = $r->input('alamat');

        $id->save();

        return redirect()->action(
            'ControllerSaya@ambilNama'
        );
    }
}
//
